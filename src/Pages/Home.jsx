import FloatingActionButton from "../Components/FloatingActionButton";
import ProductCard from "../Components/Home/ProductCard";
import Navbar from "../Components/Navbar";
import products from "../data/products.json";

const Home = () => {
  return (
    <>
      <Navbar />

      <section className="bg-navyBlue p-8 min-h-screen">
        <h2 className="text-3xl text-blueGray font-bold mb-4">
          Featured Products
        </h2>
        <div className="overflow-y-auto max-h-screen">
          <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-8">
            {products.map((product) => (
              <ProductCard
                key={product.id}
                product={product}
                onAddToCart={() => {}}
              />
            ))}
          </div>
        </div>
      </section>

      <FloatingActionButton cartItems={[]} />
    </>
  );
};

export default Home;
