// FloatingActionButton.jsx
import { useState } from "react";
import { ShoppingCartIcon } from "@heroicons/react/24/outline";
import CartSummarySidebar from "./CartSummarySidebar"; // Create CartSummarySidebar component

const FloatingActionButton = ({ cartItems }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  const toggleSidebar = () => {
    setIsSidebarOpen((state) => !state);
  };

  return (
    <div className="fixed bottom-8 right-8">
      <button
        onClick={toggleSidebar}
        className="bg-blue-500 text-white px-4 py-2 rounded-full flex items-center justify-center hover:bg-blue-600"
      >
        <ShoppingCartIcon className="h-6 w-6 mr-2" />
        Cart
      </button>
      <CartSummarySidebar
        cartItems={cartItems}
        closeSidebar={toggleSidebar}
        isSidebarOpen={isSidebarOpen}
      />
    </div>
  );
};

export default FloatingActionButton;
