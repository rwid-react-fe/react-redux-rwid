// CartSummarySidebar.jsx

import { formatPrice } from "../lib/format";

const CartSummarySidebar = ({ cartItems, closeSidebar, isSidebarOpen }) => {
  // Calculate total quantity and price
  const totalQuantity = cartItems.reduce(
    (total, item) => total + item.quantity,
    0
  );
  const totalPrice = cartItems.reduce(
    (total, item) => total + item.quantity * item.price,
    0
  );

  return (
    <div
      className={`fixed inset-y-0 ${
        isSidebarOpen ? "left-0" : "-left-96"
      } bg-white w-64 p-4 shadow-md duration-200`}
    >
      <div className="flex justify-between items-center mb-4">
        <h2 className="text-2xl font-bold">Cart Summary</h2>
        <button onClick={closeSidebar} className="text-gray-500">
          Close
        </button>
      </div>
      {cartItems.map((item) => (
        <div key={item.id} className="flex justify-between items-center mb-2">
          <p>{item.name}</p>
          <p>
            {item.quantity} x ${item.price}
          </p>
        </div>
      ))}
      <hr className="my-4" />
      <div className="flex justify-between items-center">
        <p className="font-semibold">Total Items:</p>
        <p>{totalQuantity}</p>
      </div>
      <div className="flex justify-between items-center">
        <p className="font-semibold">Total Price:</p>
        <p>{formatPrice(totalPrice)}</p>
      </div>
    </div>
  );
};

export default CartSummarySidebar;
