// ProductCard.jsx
import { useState } from "react";
import { HeartIcon } from "@heroicons/react/20/solid";
import {
  ShoppingCartIcon,
  HeartIcon as HeartIconOutlined,
} from "@heroicons/react/24/outline";

import { formatPrice } from "../../lib/format";

const ProductCard = ({ product, onAddToCart }) => {
  const [isLiked, setIsLiked] = useState(false);

  const handleLikeToggle = () => {
    setIsLiked(!isLiked);
  };

  return (
    <div className="bg-white p-4 shadow-md rounded-md">
      <img
        src={product.image}
        alt={product.name}
        className="w-full h-32 object-cover mb-4 hover:h-48 duration-200"
      />
      <div className="flex justify-between items-center mb-2">
        <h3 className="text-lg font-semibold">{product.name}</h3>
        <button
          onClick={handleLikeToggle}
          className={isLiked ? "text-red-600" : ""}
        >
          {isLiked ? (
            <HeartIcon className="h-6 w-6" />
          ) : (
            <HeartIconOutlined className="h-6 w-6" />
          )}
        </button>
      </div>
      <p className="text-gray-600 mb-4">{formatPrice(product.price)}</p>
      <button
        onClick={onAddToCart}
        className="bg-blue-500 text-white px-4 py-2 rounded-md flex items-center justify-center hover:bg-blue-600"
      >
        <ShoppingCartIcon className="h-6 w-6 mr-2" />
        Add to Cart
      </button>
    </div>
  );
};

export default ProductCard;
