// Navbar.jsx
import {
  ShoppingCartIcon,
  HeartIcon,
  UserIcon,
} from "@heroicons/react/20/solid";

const Navbar = () => {
  return (
    <nav className="bg-royalBlue p-4 text-white">
      <div className="container mx-auto flex items-center justify-between">
        <div className="text-2xl font-bold">Marketplace</div>
        <div className="flex items-center space-x-4">
          <div className="relative cursor-pointer">
            <ShoppingCartIcon className="h-6 w-6" />
            <div className="absolute -top-1 -right-1 bg-red-500 text-white text-xs w-4 rounded-full flex items-center justify-center">
              1 {/* Cart Badge */}
            </div>
          </div>
          <div className="relative cursor-pointer">
            <HeartIcon className="h-6 w-6" />
            <div className="absolute -top-1 -right-1 bg-red-500 text-white text-xs w-4 rounded-full flex items-center justify-center">
              2 {/* Heart Badge */}
            </div>
          </div>
          <UserIcon className="h-6 w-6" />
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
